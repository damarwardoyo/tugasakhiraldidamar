from django.urls import path
from .views import scrapper
app_name = 'scrapper'

urlpatterns = [
    path('', scrapper, name='home')
]
