from django.shortcuts import render
from bs4 import BeautifulSoup
import requests
from requests.adapters import HTTPAdapter
from urllib3 import Retry

response = {}


def scrapper(request):
    API_ENDPOINT = 'https://api.kurs.web.id/api/v1'
    TOKEN = 'mxXdJOBBcZLFJTA9euPI7vGqiKVROdQBnELzhSEx'
    BANK = 'bca'
    CURRENCY = 'usd'

    # response = requests.get(f'{API_ENDPOINT}?token={TOKEN}&bank={BANK}&matauang={CURRENCY}')

    # harga_beli_dollar = 0
    # if response.status_code == 200:
    #     response_JSON = response.json()
    #     harga_beli_dollar = response_JSON['beli']
    harga_beli_dollar = 14185
    dollar_str = str(harga_beli_dollar // 1000) + '.' + str(harga_beli_dollar % 1000)
    response['harga_beli_dollar'] = dollar_str

    # Halaman website yang akan discrap
    URL = "https://ews.kemendag.go.id/"

    # inisiasi membuat session baru
    session = requests.Session()
    retry = Retry(connect=5, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    # Melakukan pengecekan apakah url bisa diakses
    page_status = session.get(URL).status_code

    data_pangan = []

    # Apabila bisa diakses lanjut ke proses scrapping
    if page_status == 200:
        page = session.get(URL)
        soup = BeautifulSoup(page.content, 'html.parser')
        # mencari seluruh item yang memiliki class bernama sp2kpindexhargacontent
        items = soup.find_all(class_="sp2kpindexhargacontent")
        for i in range(18):
            raw_data_sembako = list(items[i].children)
            nama_sembako = raw_data_sembako[1].get_text()
            # harga_sembako = int(raw_data_sembako[3].get_text().split('.')[1])*1000
            price_and_unit = raw_data_sembako[3].get_text().split(' ')[1].split('/')

            raw_price = price_and_unit[0]
            unit = price_and_unit[1]

            splitted_price = raw_price.split('.')
            price_int = int(splitted_price[0]) * 1000 + int(splitted_price[1])

            harga_dollar = 0
            if harga_beli_dollar > 0:
                harga_dollar = price_int / harga_beli_dollar

            data_pangan.append({
                'nama': nama_sembako,
                'harga_rupiah': raw_price,
                'harga_dollar': round(harga_dollar, 2),
                'unit': unit
            })

        # mencari gambar untuk masing masing item
        chicken = soup.find_all(class_="sp2kpindexhargaimg")
        for i in range(18):
            raw_image_data_sembako = list(chicken[i].children)
            string_gambs = str(raw_image_data_sembako[1])
            gambar_sembako = 'https://ews.kemendag.go.id/' + string_gambs[10:-3]
            data_pangan[i]['gambar_url'] = gambar_sembako

        response['data'] = data_pangan
        return render(request, "home.html", response)
    # Apabila tidak web tidak bisa diakses, beri pesan error
    else:
        return render(request, "home.html")